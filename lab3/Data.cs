﻿//------------------------------PfPCS-----------------------------------------
//----------------------------Labwork #3--------------------------------------
//-------C#.Semaphores, Semaphores Slim, Mutexes, Events, Locks, Barriers-----
//----------------------------------------------------------------------------
//--------------------Task: A = sort(B)*d + e * T*(MO*MK)---------------------
//----------------------------------------------------------------------------
//------ Author : Butskiy Yuriy, IO - 52 group--------------------------------
//------ Date : 04.04.2018----------------------------------------------------
//----------------------------------------------------------------------------
using System;

namespace lab3
{
    class Data
    {
        private int N;
        private int P;
        private int H;

        public Data(int n, int p)
        {
            this.N = n;
            this.P = p;
            H = N / P;
        }

        public void Input_Integer(out int a)
        {
            a = 1;
        }

        public void Input_Vector(Vector<int> A)
        {
            for(int i = 0; i < N; i++)
            {
                A[i] = 1;
            }
        }

        public void Input_Matrix(Matrix<int> MA)
        {
            for(int i = 0; i < N; i++)
            {
                for(int j = 0; j < N; j++)
                {
                    MA[i, j] = 1;
                }
            }
        }

        public void Output_Vector(Vector<int> A)
        {
            if (N < 13)
            {
                Console.WriteLine();
                for(int i = 0; i < N; i++)
                {
                    Console.Write(A[i] + "  ");
                }
                Console.WriteLine();
            }
        }

        public Vector<int> Multiply_Matrix_Matrix_Vector(Matrix<int> MA, Matrix<int> MB, Vector<int> A, int k)
        {
            int cell;
            Matrix<int> matr = new Matrix<int>(N, N);
            Vector<int> result = new Vector<int>(N);
            for (int i = H* k; i < H * (1 + k); i++)
            {
                for (int j = 0; j < N; j++)
                {
                    cell = 0;
                    for (int l = 0; l < N; l++)
                    {
                        cell += MA[i,l] * MB[l,j];
                    }
                    matr[i,j] = cell;
                }
                cell = 0;
                for (int j = 0; j < N; j++)
                {
                    cell += A[i] * matr[i,j];
                }
                result[i] = cell;
            }
            return result;
        }

        public Vector<int> Multiply_Vector_Integer(Vector<int> A, int a, int k)
        {
            Vector<int> result = new Vector<int>(N);
            for (int i = H * k; i < H * (1 + k); i++)
            {
                result[i] = A[i] * a;
            }
            return result;
        }

        public void Sum_Vectors(Vector<int> A, Vector<int> B, Vector<int> C, int k)
        {
            for (int i = H * k; i < H * (1 + k); i++)
            {
                C[i] = A[i] + B[i];
            }
        }

        public void Sort_Vectors(Vector<int> A, int left, int right)
        {
            if (left == right)
                return;
            int middle = (left + right) / 2;
            Sort_Vectors(A, left, middle);
            Sort_Vectors(A, middle + 1, right);

            int i = left;
            int j = middle + 1;
            Vector<int> B = new Vector<int>(N);
            for (int step = 0; step < right - left + 1; step++)
            {
                if ((j > right) || ((i <= middle) && (A[i] < A[j])))
                {
                    B[step] = A[i];
                    i++;
                }
                else
                {
                    B[step] = A[j];
                    j++;
                }
            }
            for (int step = 0; step < right - left + 1; step++)
                A[left + step] = B[step];
        }
    }

    class Vector<T>
    {
        private int n;
        private T[] internalarr;

        public Vector(int n)
        {
            this.n = n;
            internalarr = new T[n];
        }

        public Vector(Vector<T> other)
        {
            this.n = other.n;
            internalarr = new T[n];
            other.internalarr.CopyTo(this.internalarr, 0);
        }
        
        public T this[int i]
        {
            get
            {
                return internalarr[i];
            }
            set
            {
                internalarr[i] = value;
            }
        }
    }

    class Matrix<T>
    {
        private int m, n;
        private T[,] internalarr;

        public Matrix(int m, int n)
        {
            this.m = m;
            this.n = n;
            internalarr = new T[m,n];
        }

        public Matrix(Matrix<T> other)
        {
            this.m = other.m;
            this.n = other.n;
            internalarr = new T[m,n];
            other.internalarr.CopyTo(internalarr, 0);
        }

        public T this[int i, int j]
        {
            get
            {
                return internalarr[i,j];
            }
            set
            {
                internalarr[i, j] = value;
            }
        }
    }
}