﻿//------------------------------PfPCS-----------------------------------------
//----------------------------Labwork #3--------------------------------------
//-------C#.Semaphores, Semaphores Slim, Mutexes, Events, Locks, Barriers-----
//----------------------------------------------------------------------------
//--------------------Task: A = sort(B)*d + e * T*(MO*MK)---------------------
//----------------------------------------------------------------------------
//------ Author : Butskiy Yuriy, IO - 52 group--------------------------------
//------ Date : 04.04.2018----------------------------------------------------
//----------------------------------------------------------------------------
using System;
using System.Threading;

namespace lab3
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Labwork #3 started");
            LabThreads lt = new LabThreads(12, 6);

            Thread[] threads =
            {
                new Thread(lt.T1),
                new Thread(lt.T2),
                new Thread(lt.T3),
                new Thread(lt.T4),
                new Thread(lt.T5),
                new Thread(lt.T6)
            };

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Start();
            }
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
            }
            Console.WriteLine("Labwork #3 finished");
            Console.ReadKey();
        }
    }

    class LabThreads
    {
        private int N;
        private int P;
        private int H;

        private Data data;

        private int d, e;
        private Vector<int> A;
        private Vector<int> B;
        private Vector<int> T;
        private Matrix<int> MO;
        private Matrix<int> MK;

        private Semaphore Sem_sort1 = new Semaphore(0, 1);
        private Semaphore Sem_sort2 = new Semaphore(0, 1);
        private Semaphore Sem_sort3 = new Semaphore(0, 1);
        private Semaphore Sem_sort1_1 = new Semaphore(0, 1);
        private Semaphore Sem_sort1_2 = new Semaphore(0, 1);
        private Semaphore[] Sem_end =
        {
            new Semaphore(0, 1),
            new Semaphore(0, 1),
            new Semaphore(0, 1),
            new Semaphore(0, 1),
            new Semaphore(0, 1)
        };

        private Barrier Sync_Bar = new Barrier(6);

        private SemaphoreSlim Sem_Slim = new SemaphoreSlim(1, 1);

        private Mutex Mute = new Mutex();

        private Object Key_Lock = new Object();

        private EventWaitHandle Event1_23456 = new ManualResetEvent(false);
        private EventWaitHandle Event3_12456 = new ManualResetEvent(false);
        private EventWaitHandle Event4_12356 = new ManualResetEvent(false);

        public LabThreads(int N, int P)
        {
            this.N = N;
            this.P = P;
            H = N / P;
            data = new Data(N, P);
            A = new Vector<int>(N);
            B = new Vector<int>(N);
            T = new Vector<int>(N);
            MO = new Matrix<int>(N, N);
            MK = new Matrix<int>(N, N);
        }

        public void T1()
        {
            int k = 0;
            int d1;
            int e1;
            Matrix<int> MK1 = new Matrix<int>(N, N);

            Console.WriteLine("T1 started");

            //1. Input values of B and MO
            data.Input_Vector(B);
            data.Input_Matrix(MO);

            //2. Tell T2-T6 about input
            Event1_23456.Set();
            //3. Wait for input in T3
            Event3_12456.WaitOne();
            //4. Wait for input in T4
            Event4_12356.WaitOne();

            //5. Calculating of local sort
            data.Sort_Vectors(B, k * H, H * (1 + k) - 1);
            //6. Tell T2 about end of local sort
            Sem_sort1.Release();
            //7. Tell and wait of global sort in T2
            Sync_Bar.SignalAndWait();

            //8. Copying of local variables
            lock(Key_Lock)
            {
                d1 = d;
            }
            Sem_Slim.Wait();
            e1 = e;
            Sem_Slim.Release();
            Mute.WaitOne();
            MK1 = MK;
            Mute.ReleaseMutex();

            //9. Calculating of function
            data.Sum_Vectors(data.Multiply_Vector_Integer(B, d1, k), data.Multiply_Vector_Integer(data.Multiply_Matrix_Matrix_Vector(MO, MK1, T, k), e1, k), A, k);

            //10. Wait for end of calculations
            WaitHandle.WaitAll(Sem_end);
            //11. Output result
            data.Output_Vector(A);

            Console.WriteLine("T1 finished");
        }

        public void T2()
        {
            int k = 1;
            int d2;
            int e2;
            Matrix<int> MK2 = new Matrix<int>(N, N);

            Console.WriteLine("T2 started");

            //1. Wait for input in T1
            Event1_23456.WaitOne();
            //2. Wait for input in T3
            Event3_12456.WaitOne();
            //3. Wait for input in T4
            Event4_12356.WaitOne();

            //4. Calculating of local sort
            data.Sort_Vectors(B, k * H, H * (1 + k) - 1);
            //5. Wait for end of local sort in T1
            Sem_sort1.WaitOne();
            //6. Calculating of local sort step 2
            data.Sort_Vectors(B, 0, H * (1 + k) - 1);
            //7. Wait for end of local sort step 2 in T4
            Sem_sort1_1.WaitOne();
            //8. Calculating of local sort step 4
            data.Sort_Vectors(B, 0, H * (1 + 3 * k) - 1);
            //9. Wait for end of local sort step 2 in T6
            Sem_sort1_2.WaitOne();
            //10. Calculating of global sort
            data.Sort_Vectors(B, 0, N - 1);
            //11. Tell T1, T3-T6 about end of global sort
            Sync_Bar.SignalAndWait();

            //12. Copying local variables
            Sem_Slim.Wait();
            e2 = e;
            Sem_Slim.Release();
            lock (Key_Lock)
            {
                d2 = d;
            }
            Mute.WaitOne();
            MK2 = MK;
            Mute.ReleaseMutex();

            //13. Calculating of function
            data.Sum_Vectors(data.Multiply_Vector_Integer(B, d2, k), data.Multiply_Vector_Integer(data.Multiply_Matrix_Matrix_Vector(MO, MK2, T, k), e2, k), A, k);

            //14. Tell T1 about end of calculation
            Sem_end[0].Release();

            Console.WriteLine("T2 finished");
        }

        public void T3()
        {
            int k = 2;
            int d3;
            int e3;
            Matrix<int> MK3 = new Matrix<int>(N, N);

            Console.WriteLine("T3 started");

            //1. Input values of d and MK
            data.Input_Integer(out d);
            data.Input_Matrix(MK);
            //2. Tell T1,T2,T4-T6 about input
            Event3_12456.Set();
            //3. Wait for input in T1
            Event1_23456.WaitOne();
            //4. Wait for input in T4
            Event4_12356.WaitOne();

            //5. Calculating of local sort
            data.Sort_Vectors(B, k * H, H * (1 + k) - 1);
            //6. Tell T4 about end of local sort
            Sem_sort2.Release();
            //7. Tell and wait of global sort in T2
            Sync_Bar.SignalAndWait();

            //8. Copying local variables
            Mute.WaitOne();
            MK3 = MK;
            Mute.ReleaseMutex();
            lock (Key_Lock)
            {
                d3 = d;
            }
            Sem_Slim.Wait();
            e3 = e;
            Sem_Slim.Release();

            //9. Calculating of function
            data.Sum_Vectors(data.Multiply_Vector_Integer(B, d3, k), data.Multiply_Vector_Integer(data.Multiply_Matrix_Matrix_Vector(MO, MK3, T, k), e3, k), A, k);

            //10. Tell T1 about end of calculation
            Sem_end[1].Release();

            Console.WriteLine("T3 finished");
        }

        public void T4()
        {
            int k = 3;
            int d4;
            int e4;
            Matrix<int> MK4 = new Matrix<int>(N, N);

            Console.WriteLine("T4 started");

            //1. Input values of e and T
            data.Input_Integer(out e);
            data.Input_Vector(T);
            //2. Tell T1-T3,T5,T6 about input
            Event4_12356.Set();
            //3. Wait input in T1
            Event1_23456.WaitOne();
            //4. Wait input in T3
            Event3_12456.WaitOne();

            //5. Calculating of local sort
            data.Sort_Vectors(B, k * H, H * (1 + k) - 1);
            //6. Wait for end of local sort in T3
            Sem_sort2.WaitOne();
            //7. Calculating of local sort step 2
            data.Sort_Vectors(B, (k - 1) * H, H * (1 + k) - 1);
            //8. Tell T2 about end of local sort step 2
            Sem_sort1_1.Release();
            //9. Tell and wait of global sort in T2
            Sync_Bar.SignalAndWait();

            //10. Copying local variables
            Mute.WaitOne();
            MK4 = MK;
            Mute.ReleaseMutex();
            Sem_Slim.Wait();
            e4 = e;
            Sem_Slim.Release();
            lock (Key_Lock)
            {
                d4 = d;
            }

            //11. Calculating of function
            data.Sum_Vectors(data.Multiply_Vector_Integer(B, d4, k), data.Multiply_Vector_Integer(data.Multiply_Matrix_Matrix_Vector(MO, MK4, T, k), e4, k), A, k);

            //12. Tell T1 about end of calculation
            Sem_end[2].Release();

            Console.WriteLine("T4 finished");
        }

        public void T5()
        {
            int k = 4;
            int d5;
            int e5;
            Matrix<int> MK5 = new Matrix<int>(N, N);

            Console.WriteLine("T5 started");

            //1. Wait for input in T1
            Event1_23456.WaitOne();
            //2. Wait for input in T3
            Event3_12456.WaitOne();
            //3. Wait for input in T4
            Event4_12356.WaitOne();

            //4. Calculating of local sort
            data.Sort_Vectors(B, k * H, H * (1 + k) - 1);
            //5. Tell T6 about end of local sort
            Sem_sort3.Release();
            //6. Tell and wait of global sort in T2
            Sync_Bar.SignalAndWait();

            //7. Copying local variables
            lock (Key_Lock)
            {
                d5 = d;
            }
            Mute.WaitOne();
            MK5 = MK;
            Mute.ReleaseMutex();
            Sem_Slim.Wait();
            e5 = e;
            Sem_Slim.Release();

            //8. Calculating of function
            data.Sum_Vectors(data.Multiply_Vector_Integer(B, d5, k), data.Multiply_Vector_Integer(data.Multiply_Matrix_Matrix_Vector(MO, MK5, T, k), e5, k), A, k);

            //9. Tell T1 about end of calculation
            Sem_end[3].Release();

            Console.WriteLine("T5 finished");
        }
        
        public void T6()
        {
            int k = 5;
            int d6;
            int e6;
            Matrix<int> MK6 = new Matrix<int>(N, N);

            Console.WriteLine("T6 started");

            //1. Wait for input in T1
            Event1_23456.WaitOne();
            //2. Wait for input in T3
            Event3_12456.WaitOne();
            //3. Wait for input in T4
            Event4_12356.WaitOne();

            //4. Calculating of local sort
            data.Sort_Vectors(B, k * H, H * (1 + k) - 1);
            //5. Wait end of local sort in T5
            Sem_sort3.WaitOne();
            //6. Calculating of local sort step 2
            data.Sort_Vectors(B, (k - 1) * H, H * (1 + k) - 1);
            //7. Tell T2 about end of local sort step 2
            Sem_sort1_2.Release();
            //8. Tell and wait of global sort in T2
            Sync_Bar.SignalAndWait();

            //9. Copying local variables
            Sem_Slim.Wait();
            e6 = e;
            Sem_Slim.Release();
            lock (Key_Lock)
            {
                d6 = d;
            }
            Mute.WaitOne();
            MK6 = MK;
            Mute.ReleaseMutex();

            //10. Calculating of function
            data.Sum_Vectors(data.Multiply_Vector_Integer(B, d6, k), data.Multiply_Vector_Integer(data.Multiply_Matrix_Matrix_Vector(MO, MK6, T, k), e6, k), A, k);

            //11. Tell T1 about end of calculation
            Sem_end[4].Release();

            Console.WriteLine("T6 finished");
        }
    }
}
